# Changelog

This file reflects the evolution of the Game Tools library, from one version to
another.

## Version 1.0

+ Provide a `Randomizer` class to allow both randomness and reproducibility
+ Provide some time handling to make easier execution of postponed tasks
