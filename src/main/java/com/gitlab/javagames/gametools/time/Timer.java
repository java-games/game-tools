/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.time;

import static com.gitlab.javagames.gametools.time.Sleeper.sleepFor;

/**
 * The Timer class enables to perform an action after a given amount of time,
 * asynchronously.
 * 
 * You can either instantiate an object of this class if you need to re-use it, or invoke
 * the static method {@link #after(long, Runnable)} if you prefer.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Timer {

    /**
     * The delay before executing the action.
     */
    private final long delay;

    /**
     * The action to execute.
     */
    private final Runnable action;

    /**
     * Creates a new Timer.
     * 
     * @param delay The delay before executing the action.
     * @param action The action to execute.
     */
    public Timer(long delay, Runnable action) {
        this.delay = delay;
        this.action = action;
    }

    /**
     * Starts this timer, i.e. executes asynchronously the associated action after a sleep
     * of the associated delay.
     */
    public void start() {
        after(delay, action);
    }

    /**
     * Executes asynchronously the given action after a sleep of the associated delay.
     * 
     * @param delay The delay before executing the action.
     * @param action The action to execute.
     */
    public static void after(long delay, Runnable action) {
        new Thread(() -> {
            sleepFor(delay);
            action.run();
        }).start();
    }

}
