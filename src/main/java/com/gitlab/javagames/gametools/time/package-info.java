/**
 * The com.gitlab.javagames.gametools.time package provides some useful tools to handle
 * time in your game applications.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.javagames.gametools.time;
