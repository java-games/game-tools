/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.time;

/**
 * The Ticker class enables to repeatedly and asynchronously execute an action, with a
 * given delay between each start of execution.
 * A new execution of the task is performed even if the previous execution is not yet
 * finished.
 * 
 * This is a decorator for a {@link Timer}.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Ticker {

    /**
     * The timer used to delay the execution.
     */
    private Timer timer;

    /**
     * If this ticker has started.
     */
    private volatile boolean started;

    /**
     * Creates a new Ticker.
     * 
     * @param delay The delay between each execution of the action.
     * @param action The action to execute.
     */
    public Ticker(long delay, Runnable action) {
        this.timer = new Timer(delay, () -> {
            if (started) {
                // Starting the timer for the next execution.
                timer.start();
            }

            // Executing the action.
            action.run();
        });
    }

    /**
     * Starts this ticker.
     * 
     * @throws IllegalStateException If this ticker has already started.
     */
    public synchronized void start() {
        if (started) {
            throw new IllegalStateException("This ticker has already started.");
        }
        started = true;
        timer.start();
    }

    /**
     * Stops this ticker.
     * 
     * @throws IllegalStateException If this ticker has not started yet.
     */
    public synchronized void stop() {
        if (!started) {
            throw new IllegalStateException("This ticker has not been started.");
        }
        started = false;
    }

}
