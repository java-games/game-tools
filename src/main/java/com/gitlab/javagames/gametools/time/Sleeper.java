/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.time;

/**
 * The Sleeper class enables to sleep for a given amount of time, without taking care of
 * interruptions.
 * 
 * You can either instantiate an object of this class if you need to perform several
 * sleeps of the same duration, or invoke the static method {@link #sleepFor(long)} if you
 * prefer.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Sleeper {

    /**
     * The duration of the sleeps.
     */
    private final long duration;

    /**
     * Creates a new Sleeper.
     * 
     * @param duration The duration of the sleeps.
     */
    public Sleeper(long duration) {
        this.duration = duration;
    }

    /**
     * Sleeps for the associated amount of time.
     */
    public void sleep() {
        sleepFor(duration);
    }

    /**
     * Sleeps for the given amount of time.
     * 
     * @param duration The duration of the sleep.
     */
    public static void sleepFor(long duration) {
        try {
            Thread.sleep(duration);

        } catch (InterruptedException e) {
            // Re-interrupting the current thread.
            Thread.currentThread().interrupt();
        }
    }

}
