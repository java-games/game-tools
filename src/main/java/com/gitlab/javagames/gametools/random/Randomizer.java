/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.random;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * The Randomizer class allows to introduce randomness in your applications, while
 * enabling reproducibility for debugging.
 * 
 * To do so, you can for example log the seed used each time your application starts up,
 * and later set it to the same value to reproduce what happened.
 * By default, the seed is initialized to the current time (in nanoseconds since the
 * Epoch).
 * 
 * It provides a set of static methods, to easily share an instance of {@link Random}
 * in the whole program.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Randomizer {

    /**
     * The seed used to initialize the pseudo-random number generator.
     */
    private static long seed = System.nanoTime();

    /**
     * The {@link Random} instance used to generate pseudo-random numbers.
     */
    public static final Random RANDOM = new Random(seed);

    /**
     * Disables instantiation.
     */
    private Randomizer() {
        throw new AssertionError("No Randomizer instances for you!");
    }

    /**
     * Gives the seed to use to produce pseudo-random numbers.
     * 
     * @return The seed in use.
     */
    public static long getSeed() {
        return seed;
    }

    /**
     * Sets the seed to use to produce pseudo-random numbers.
     * If you want to use it, it should be done once and for all, at the beginning
     * of the execution, to enable reproducibility.
     * 
     * @param seed The new seed to use.
     */
    public static void setSeed(long seed) {
        RANDOM.setSeed(seed);
        Randomizer.seed = seed;
    }

    /**
     * Returns a pseudo-random, uniformly distributed boolean.
     * 
     * @return A pseudo-random boolean.
     * 
     * @see Random#nextBoolean()
     */
    public static boolean randomBoolean() {
        return RANDOM.nextBoolean();
    }

    /**
     * Returns a pseudo-random, uniformly distributed integer among all the
     * possible values.
     * 
     * @return A pseudo-random integer.
     * 
     * @see Random#nextInt()
     */
    public static int randomInteger() {
        return RANDOM.nextInt();
    }

    /**
     * Returns a pseudo-random, uniformly distributed integer, between {@code 0}
     * (inclusive) and {@code upperBound} (exclusive).
     * 
     * @param upperBound The upper bound of the number to generate (exclusive).
     * 
     * @return A pseudo-random integer.
     * 
     * @see Random#nextInt(int)
     */
    public static int randomInteger(int upperBound) {
        return RANDOM.nextInt(upperBound);
    }

    /**
     * Returns a pseudo-random, uniformly distributed integer, between {@code lowerBound}
     * (inclusive) and {@code upperBound} (exclusive).
     * 
     * @param lowerBound The lower bound of the number to generate (inclusive).
     * @param upperBound The upper bound of the number to generate (exclusive).
     * 
     * @return A pseudo-random integer.
     * 
     * @see Random#nextInt(int)
     * @see #randomInteger()
     */
    public static int randomInteger(int lowerBound, int upperBound) {
        return randomInteger(upperBound - lowerBound) + lowerBound;
    }

    /**
     * Returns a pseudo-random, uniformly distributed double between {@code 0.0d}
     * (inclusive) and {@code 1.0d} (exclusive).
     * 
     * @return A pseudo-random double.
     * 
     * @see Random#nextDouble()
     */
    public static double randomDouble() {
        return RANDOM.nextDouble();
    }

    /**
     * Returns a pseudo-random, uniformly distributed double, between {@code 0.0}
     * (inclusive) and {@code upperBound} (exclusive).
     * 
     * @param upperBound The upper bound of the number to generate (exclusive).
     * 
     * @return A pseudo-random double.
     * 
     * @see Random#nextDouble()
     * @see #randomDouble()
     */
    public static double randomDouble(double upperBound) {
        return upperBound * randomDouble();
    }

    /**
     * Returns a pseudo-random, uniformly distributed double, between {@code lowerBound}
     * (inclusive) and {@code upperBound} (exclusive).
     * 
     * @param lowerBound The lower bound of the number to generate (inclusive).
     * @param upperBound The upper bound of the number to generate (exclusive).
     * 
     * @return A pseudo-random double.
     * 
     * @see Random#nextDouble()
     * @see #randomDouble(double)
     */
    public static double randomDouble(double lowerBound, double upperBound) {
        return randomDouble(upperBound - lowerBound) + lowerBound;
    }

    /**
     * Shuffles the given list.
     * 
     * @param aList The list to shuffle.
     * 
     * @see Collections#shuffle(List, Random)
     */
    public static void shuffle(List<?> aList) {
        Collections.shuffle(aList, RANDOM);
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(boolean[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            boolean tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(byte[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            byte tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(short[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            short tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(char[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            char tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(int[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            int tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(float[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            float tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(double[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            double tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Shuffles the given array.
     * The algorithm is the same as the one used in
     * {@link Collections#shuffle(List, Random)}.
     * 
     * @param anArray The array to shuffle.
     */
    public static void shuffle(Object[] anArray) {
        for (int i = anArray.length; i > 1; i--) {
            int toSwap = randomInteger(i);
            Object tmp = anArray[i - 1];
            anArray[i - 1] = anArray[toSwap];
            anArray[toSwap] = tmp;
        }
    }

    /**
     * Creates a dice with 6 sides.
     * 
     * @return The created dice.
     */
    public static Dice dice() {
        return dice(6);
    }

    /**
     * Creates a dice with the given number of sides.
     * 
     * @param sides The number of sides for the dice.
     * 
     * @return The created dice.
     * 
     * @throws IllegalArgumentException If {@code sides} is strictly less than 2.
     */
    public static Dice dice(int sides) {
        if (sides < 2) {
            // A dice with the given number of sides does not exist.
            throw new IllegalArgumentException("Illegal number of sides for a dice: " + sides);
        }

        return () -> randomInteger(1, sides + 1);
    }

}
