/**
 * The com.gitlab.javagames.gametools.random package contains classes which allow to
 * introduce randomness in your game applications, while enabling reproducibility for
 * debugging.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.javagames.gametools.random;
