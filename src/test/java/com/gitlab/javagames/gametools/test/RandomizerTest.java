/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.gitlab.javagames.gametools.random.Randomizer;

/**
 * The RandomizerTest is the test class for {@link Randomizer}.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class RandomizerTest {
    
    /**
     * The number of times to repeat the tests.
     */
    private static final int REPEAT_TESTS = 10000;
    
    /**
     * Resets the seed before each test.
     */
    @Before
    public void setUp() {
        Randomizer.setSeed(42);
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#randomInteger(int)}.
     */
    @Test
    public void testRandomIntegerWithUpperBound() {
        for (int i = 0; i < REPEAT_TESTS; i++) {
            int rnd = Randomizer.randomInteger(42);
            assertTrue(rnd >= 0);
            assertTrue(rnd < 42);
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#randomInteger(int, int)}.
     */
    @Test
    public void testRandomIntegerWithLowerAndUpperBound() {
        for (int i = 0; i < REPEAT_TESTS; i++) {
            int rnd = Randomizer.randomInteger(12, 42);
            assertTrue(rnd >= 12);
            assertTrue(rnd < 42);
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#randomDouble()}.
     */
    @Test
    public void testRandomDouble() {
        for (int i = 0; i < REPEAT_TESTS; i++) {
            double rnd = Randomizer.randomDouble();
            assertTrue(Double.compare(rnd, 0.) >= 0);
            assertTrue(Double.compare(rnd, 1.) < 0);
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#randomDouble(double)}.
     */
    @Test
    public void testRandomDoubleWithUpperBound() {
        for (int i = 0; i < REPEAT_TESTS; i++) {
            double rnd = Randomizer.randomDouble(42.);
            assertTrue(Double.compare(rnd, 0.) >= 0);
            assertTrue(Double.compare(rnd, 42.) < 0);
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#randomDouble(double, double)}.
     */
    @Test
    public void testRandomDoubleWithLowerAndUpperBound() {
        for (int i = 0; i < REPEAT_TESTS; i++) {
            double rnd = Randomizer.randomDouble(12., 42.);
            assertTrue(Double.compare(rnd, 12.) >= 0);
            assertTrue(Double.compare(rnd, 42.) < 0);
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(byte[])}.
     */
    @Test
    public void testShuffleByteArray() {
        byte[] array = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            byte[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(short[])}.
     */
    @Test
    public void testShuffleShortArray() {
        short[] array = new short[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            short[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(char[])}.
     */
    @Test
    public void testShuffleCharArray() {
        char[] array = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            char[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(int[])}.
     */
    @Test
    public void testShuffleIntArray() {
        int[] array = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            int[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(float[])}.
     */
    @Test
    public void testShuffleFloatArray() {
        float[] array = new float[] { 0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 9.f };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            float[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(double[])}.
     */
    @Test
    public void testShuffleDoubleArray() {
        double[] array = new double[] { 0., 1., 2., 3., 4., 5., 6., 7., 9. };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            double[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.random.Randomizer#shuffle(java.lang.Object[])}.
     */
    @Test
    public void testShuffleObjectArray() {
        String[] array = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i" };
        for (int i = 0; i < REPEAT_TESTS; i++) {
            String[] clonedArray = array.clone();
            Randomizer.shuffle(clonedArray);
            assertFalse(Arrays.equals(array, clonedArray));
        }
    }

}
