/**
 * The com.gitlab.javagames.gametools.test package contains the classes used to test the
 * library.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.javagames.gametools.test;
