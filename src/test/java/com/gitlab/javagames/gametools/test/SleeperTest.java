/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.gitlab.javagames.gametools.time.Sleeper;

/**
 * The SleeperTest is the test class for {@link Sleeper}.
 * 
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class SleeperTest {

    /**
     * Test method for {@link com.gitlab.javagames.gametools.time.Sleeper#sleep()}.
     */
    @Test
    public void testSleep() {
        Sleeper sleeper = new Sleeper(1000);
        for (int i = 0; i < 5; i++) {
            checkSleep(sleeper::sleep, 1000);
        }
    }

    /**
     * Test method for {@link com.gitlab.javagames.gametools.time.Sleeper#sleepFor(long)}.
     */
    @Test
    public void testSleepFor() {
        checkSleep(() -> Sleeper.sleepFor(2000), 2000);
    }

    /**
     * Checks if the given sleeping action lasts at least the given time.
     * 
     * @param sleep The sleep to check.
     * @param expectedTime The expected time of the sleep.
     */
    private void checkSleep(Runnable sleep, long expectedTime) {
        long before = System.currentTimeMillis();
        sleep.run();
        long after = System.currentTimeMillis();
        assertTrue(after - before >= expectedTime);
    }

}
