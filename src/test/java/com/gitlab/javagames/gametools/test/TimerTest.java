/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gametools.test;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.javagames.gametools.time.Sleeper;
import com.gitlab.javagames.gametools.time.Timer;

/**
 * The TimerTest is the test class for {@link Timer}.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class TimerTest {

    /**
     * Test method for an instance of {@link com.gitlab.javagames.gametools.time.Timer}.
     */
    @Test
    public void testTimerExecution() {
        AtomicInteger integer = new AtomicInteger();
        Timer timer = new Timer(500, integer::incrementAndGet);
        timer.start();
        Sleeper.sleepFor(1000);
        assertEquals(1, integer.intValue());
    }

    /**
     * Test method for
     * {@link com.gitlab.javagames.gametools.time.Timer#after(long, java.lang.Runnable)}.
     */
    @Test
    public void testAfter() {
        AtomicInteger integer = new AtomicInteger();
        Timer.after(500, integer::incrementAndGet);
        Sleeper.sleepFor(1000);
        assertEquals(1, integer.intValue());
    }

}
