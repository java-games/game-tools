# Game Tools

[![pipeline status](https://gitlab.com/java-games/game-tools/badges/master/pipeline.svg)](https://gitlab.com/java-games/game-tools/commits/master)

## Description

This project provides a small library to enable to easily share common 
features between games.

## Overview

This section illustrates the use of some of the provided tools.
For more details, you can have a look at the 
[Javadoc](https://java-games.gitlab.io/game-tools/).

### Random tools

In many games, you may need to use random numbers.
However, if you encounter a bug, you need to be able to reproduce what 
happened in the first place to be able to correct it.

To do so, we provide the `Randomizer` class, which wraps an instance of 
[`Random`](https://docs.oracle.com/javase/10/docs/api/java/util/Random.html), 
to keep a trace of the seed in use.

Every random action can be performed through this class.

For example, you can produce random booleans, integers, or doubles, by using
one of the static methods.
You can also get instances of `Dice`, which are factored in this class.
And, eventually, you can shuffle lists and arrays.

### Time tools

To handle time in your applications, we provide some tools to make easier the
execution of postponed tasks.

#### Sleeping

The `Sleeper` class enables to perform some *sleeps* in your code, without
taking care of interruptions.

In other words, you can replace this snippet:

```java
try {
    Thread.sleep(DURATION);
    
} catch (InterruptedException e) {
    // Do something with e...
    Thread.currentThread().interrupt();
}
```

By this one:

```java
Sleeper.sleepFor(DURATION);
```

Note that if you want to perform multiple sleeps of the same duration, you can
also instantiate a `Sleeper`, and then ask it to sleep.

```java
Sleeper sleeper = new Sleeper(DURATION);
sleeper.sleep();
```

#### Scheduling Tasks

When you want a task to be performed after a given amount of time, you can use
the `Timer` class.

```java
Timer.after(5000, () -> System.out.println("Hello, World!"));
```

By calling this method, you will print `"Hello, World!"` after 5 seconds, 
asynchronously (that is, in an other thread).
This method returns immediately.

Once again, you can also instantiate a `Timer`, if you want to reuse it.

```java
Timer timer = new Timer(5000, () -> System.out.println("Hello, World!"));
timer.start(); // Also returns immediately.
```

#### Repeating Tasks

If you want to regularly repeat an action, you can use the `Ticker` class.

```java
Ticker ticker = new Ticker(1000, () -> System.out.println("tick"));
ticker.start();
```

This snippet of code prints `"tick"` each second.
This task is performed in an other thread, after the call to `ticker.start()` 
(which returns immediately).
To stop the ticker, just call `ticker.stop()`.
